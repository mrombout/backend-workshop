// +build mage

package main

import (
	"os"

	"github.com/magefile/mage/sh"
)

// mg contains helpful utility functions, like Deps

func Generate() {
	sh.Run("swagger", "generate", "server",
		"--name", "taskapi",
		"--spec", "api/swagger.yml",
		"--model-package", "swagger",
		"-C", "../generator/simple-server.yml")
}

func GenerateClean() {
	os.RemoveAll("cmd")
	os.RemoveAll("models")
	os.RemoveAll("model")
	os.RemoveAll("restapi")
	os.RemoveAll("internal")
	os.RemoveAll("configure_task.go")
	os.RemoveAll("doc.go")
	os.RemoveAll("embedded_spec.go")
	os.RemoveAll("server.go")
}
