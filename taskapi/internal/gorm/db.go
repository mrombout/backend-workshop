package gorm

import gormdep "github.com/jinzhu/gorm"

type DB struct {
	gormdep.DB
}
