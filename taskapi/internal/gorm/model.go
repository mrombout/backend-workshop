package gorm

import (
	"time"
)

type Task struct {
	ID        int64 `gorm:"primary_key"`
	Title     string
	Completed bool
	Order     int64
	CreatedAt time.Time
	UpdatedAt time.Time
}
