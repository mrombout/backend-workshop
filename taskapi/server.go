package taskapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/gorilla/mux"
	gormdep "github.com/jinzhu/gorm"
	"gitlab.com/mrombout/backend-workshop/taskapi/internal/gorm"
	"gitlab.com/mrombout/backend-workshop/taskapi/internal/swagger"
)

// TodoAPI provides a RESTful API for managing Todo's.
type TodoAPI struct {
	DB *gormdep.DB
}

// GetTasks returns a list of all registered tasks, whether they are completed or not.
func (api *TodoAPI) GetTasks(response http.ResponseWriter, request *http.Request) {
	tasks := []gorm.Task{}
	api.DB.Find(&tasks)

	taskResponses := []swagger.TaskResponse{}
	for _, task := range tasks {
		taskResponses = append(taskResponses, newModelTaskResponse(task))
	}

	json.NewEncoder(response).Encode(taskResponses)
}

// CreateTask creates a new task.
func (api *TodoAPI) CreateTask(response http.ResponseWriter, request *http.Request) {
	var taskCreate swagger.TaskCreateRequest
	_ = json.NewDecoder(request.Body).Decode(&taskCreate)

	if err := taskCreate.Validate(strfmt.Default); err != nil {
		errResponse := swagger.Error{
			Code:    400,
			Message: err.Error(),
		}

		json.NewEncoder(response).Encode(errResponse)
		return
	}

	task := gorm.Task{
		Title:     *taskCreate.Title,
		Completed: false,
		Order:     taskCreate.Order,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	api.DB.Create(&task)

	response.WriteHeader(http.StatusCreated)
	json.NewEncoder(response).Encode(newModelTaskResponse(task))
}

// GetTask returns a task with the given id.
func (api *TodoAPI) GetTask(response http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)

	id := params["id"]
	task := gorm.Task{}
	if api.DB.First(&task, id).RecordNotFound() {
		errResponse := swagger.Error{
			Code:    404,
			Message: fmt.Sprintf("Task '%s' is not found.", id),
		}

		json.NewEncoder(response).Encode(errResponse)
		return
	}

	json.NewEncoder(response).Encode(newModelTaskResponse(task))
}

// UpdateTask updates an existing task with the provided data.
func (api *TodoAPI) UpdateTask(response http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)

	var updateTask swagger.TaskUpdateRequest
	json.NewDecoder(request.Body).Decode(&updateTask)

	if err := updateTask.Validate(strfmt.Default); err != nil {
		errResponse := swagger.Error{
			Code:    400,
			Message: err.Error(),
		}

		json.NewEncoder(response).Encode(errResponse)
		return
	}

	id := params["id"]
	var task gorm.Task
	err := api.DB.Where("id = ?", id).First(&task).Error
	if err != nil {
		errResponse := swagger.Error{
			Code:    404,
			Message: fmt.Sprintf("Task '%s' is not found.", id),
		}

		json.NewEncoder(response).Encode(errResponse)
		return
	}

	if updateTask.Title != nil {
		task.Title = *updateTask.Title
	}

	if updateTask.Completed != nil {
		task.Completed = *updateTask.Completed
	}

	if updateTask.Order != nil {
		task.Order = *updateTask.Order
	}

	err = api.DB.Save(&task).Error
	if err != nil {
		errResponse := swagger.Error{
			Code:    404,
			Message: "Task '%s' could not be saved.",
		}

		json.NewEncoder(response).Encode(errResponse)
		return
	}

	json.NewEncoder(response).Encode(newModelTaskResponse(task))
}

// DeleteTask deletes a task.
func (api *TodoAPI) DeleteTask(response http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)

	var task gorm.Task
	api.DB.First(&task, params["id"])

	api.DB.Delete(&task)
}

// DeleteTasks deletes all tasks.
func (api *TodoAPI) DeleteTasks(response http.ResponseWriter, request *http.Request) {
	api.DB.Delete(gorm.Task{})
}

func newModelTaskResponse(task gorm.Task) swagger.TaskResponse {
	url := fmt.Sprintf("http://localhost:8090/api/tasks/%d", task.ID)
	createdAt := strfmt.DateTime(task.CreatedAt)
	updatedAt := strfmt.DateTime(task.UpdatedAt)

	return swagger.TaskResponse{
		ID:        &task.ID,
		Title:     &task.Title,
		Completed: &task.Completed,
		URL:       &url,
		Order:     &task.Order,
		CreatedAt: &createdAt,
		UpdatedAt: &updatedAt,
	}
}
