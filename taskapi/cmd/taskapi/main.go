package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	gormdep "github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mrombout/backend-workshop/taskapi"
	"gitlab.com/mrombout/backend-workshop/taskapi/internal/gorm"
)

func main() {
	dbHost := os.Getenv("DB_HOST")
	if dbHost == "" {
		dbHost = "127.0.0.1"
	}

	dbPort := os.Getenv("DB_PORT")
	if dbPort == "" {
		dbPort = "5432"
	}

	db, err := gormdep.Open("postgres", fmt.Sprintf("host=%s port=%s user=postgres dbname=taskapi sslmode=disable", dbHost, dbPort))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.AutoMigrate(&gorm.Task{})

	server := taskapi.TodoAPI{
		DB: db,
	}

	router := mux.NewRouter()
	router.Use(requestLoggerMiddleware)

	router.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {
		response.Header().Set("Content-Type", "text/html; charset=utf-8")

		fmt.Fprint(response, "There's nothing here. Check out <a href='/tasks'>tasks</a> though.")
	})

	pathRoutes := router.PathPrefix("/tasks").Subrouter()
	pathRoutes.Use(corsMiddleware)
	pathRoutes.Use(jsonContentTypeMiddleware)
	pathRoutes.Use(requestLoggerMiddleware)

	pathRoutes.HandleFunc("", server.GetTasks).Methods("GET", "OPTIONS")
	pathRoutes.HandleFunc("", server.CreateTask).Methods("POST", "OPTIONS")
	pathRoutes.HandleFunc("", server.DeleteTasks).Methods("DELETE", "OPTIONS")
	pathRoutes.HandleFunc("/{id}", server.GetTask).Methods("GET", "OPTIONS")
	pathRoutes.HandleFunc("/{id}", server.UpdateTask).Methods("PATCH", "OPTIONS")
	pathRoutes.HandleFunc("/{id}", server.DeleteTask).Methods("DELETE", "OPTIONS")

	os.Getenv("API_PORT")

	log.Print("listening on :8081")
	log.Fatal(http.ListenAndServe(":8081", router))
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		response.Header().Set("Access-Control-Allow-Origin", "*")
		response.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS")
		response.Header().Set("Access-Control-Allow-Headers", "Content-Type")

		next.ServeHTTP(response, request)
	})
}

func jsonContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		response.Header().Set("Content-Type", "application/json; charset=utf-8")

		next.ServeHTTP(response, request)
	})
}

func requestLoggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		log.WithFields(log.Fields{
			"method": request.Method,
			"url":    request.URL,
		}).Info("new request")

		next.ServeHTTP(response, request)
	})
}
