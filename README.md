# Backend Workshop

## Prerequisites

You need to install the following software on your computer:

* [Postman](https://www.getpostman.com/apps)

The following software is already installed on the provided VM:

* [Docker](https://www.docker.com/) - a container platform.
* [WebIDE](https://github.com/Coding/WebIDE) - a simple web-based IDE for Go.
* [Go](https://golang.org/) - tools for the Go programming language.

## What are webservices?

If you want a recap of what had just been said, feel free to take a look at the [presentation](BackendPresentation.pptx).
It's right here in the repository!

## Assignment 1: Getting familiar with the tools

### Approach

* Compile and run the `hello-world` application using `go run ./hello-world/main.go`.
* Visit `<your_ip>:8081` to view the results.

### Objective

* Know how to edit, compile and run a Go web application during this workshop.
* Know where to reach running web applications.

### Result

You can now follow along with the workshop on your own cloud based development environment!

## Assignment 2: Define a REST API for Todo API

### Approach

Use Swagger 2.0 to define the specifications for the Todo REST API.

You can use https://editor.swagger.io/ to easily edit your specifications.

The Swagger 2.0 specification can be found here: https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md.

The Swagger 2.0 documentation can be found here: https://swagger.io/docs/specification/2-0/basic-structure/.

### Objective

Define a new REST-based webservice that allows management for tasks:

* Be able to get all tasks using `GET /tasks`.
* Be able to create a new task using `POST /tasks`.
* Be able to delete all tasks using `DELETE /tasks`.
* Be able to update a task using `PATCH /task/{id}`.
* Be able to delete a task using `DELETE /task/{id}`.

### Result

A `swagger.yml` file containing the Swagger 2.0 specification for your Todo API.

## Assignment 3: Implement the Todo REST API

### Approach

* Generate a server stub from your specification.
* Implement using GORM as your DAL.

### Objective

Implement all endpoints defined in your webservice specification in Go.

### Result

Fully functioning REST API that complies to your specification.

## Assignment 4: Testing the Todo REST API

### Approach

* Use Postman to automatically test the compliance of your server with your specs.
* Fix your server if you don't comply.

Postman documentation can be found here: https://www.getpostman.com/docs/v6/postman/scripts/test_scripts.

```
// example to test for status codes
pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});

// example to test for json field value
pm.test("Title 'Get milk.' is returned", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData.title).to.eql("Get milk.");
});

// example to validate response against json schema
// see http://geraintluff.github.io/tv4/ for tiny validator (tv4) examples
// see http://json-schema.org/examples.html for json schema examples
var schema = {
  "title": "Task",
  "type": "object",
  "properties": {
      "completed": {
          "type": "boolean"
      },
      "created_at": {
          "type": "string"
      },
      "id": {
          "type": "integer"
      }
  }
};

pm.test('Schema is valid', function() {
  var jsonData = pm.response.json();
  pm.expect(tv4.validate(jsonData, schema)).to.be.true;
});
```

### Objective

Implement automated tests to test the compliance with your specification.

### Result

A fully compliant server implementation for your defined X webservice specifications.

## Assignment 5: Deploying your webservice

### Approach

* Hide your webservice behind a reverse proxy.
* Wrap your webservice in a Docker container.
* Deploy your webservice to Heroku.

### Objective

Deploy your webservice to the internet to make it available for everyone!

### Result

Your webservice is now available for everyone to use.   

## Useful links

* [Zalando RESTful API and Event Scheme Guidelines](https://opensource.zalando.com/restful-api-guidelines/) - an extensive guideline of RESTful API's used by Zalando.
* [Google API Design Guide](https://cloud.google.com/apis/design/) - the api design guide by Google.